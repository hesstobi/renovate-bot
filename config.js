module.exports = {
  autodiscover: true,
  endpoint: 'https://gitlab.com/api/v4/',
  gitAuthor: "Renovate Bot <renovate@tobiashess.de>",
  onboarding: true,
  onboardingConfig: {
    extends: [
        'config:base'
    ],
    automerge: true,
    major: {
        automerge: false
    },
    labels: ["bot::renovate", "category::dependency"]
  },
  platform: 'gitlab',
  requireConfig: true,
  logLevel: "info",
  allowScripts: true,
  exposeAllEnv: true
};
